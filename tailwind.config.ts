import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
        'hero-bg': "url('/hero_bg.png')"
      },
      colors: {
        primary: {
          DEFAULT: "#ff603c"
        },
        text: {
          DEFAULT: "#292D33"
        },
        "light-white": {
          DEFAULT: "#e5e7eb"
        },
        "dark-white": {
          DEFAULT: "#F2F8FF"
        },
        "linear-youtube": {
          DEFAULT: "linear-gradient(90deg, #f96 0%, #ff5e62 100%)"
        }
      },
      screens: {
        'xs': '400px',
        'xmd': '900px'
      },
      dropShadow: {
        '6xl': '0px 0px 15px 4px rgba(255, 123, 105, 1)',
      },
      animation: {
        slide: "slide 2.5s linear infinite",
      },
      keyframes: {
        slide: {
          "0%": { transform: "translateY(100%)", opacity: "0.1" },
          "15%": { transform: "translateY(0)", opacity: "1" },
          "30%": { transform: "translateY(0)", opacity: "1" },
          "45%": { transform: "translateY(-100%)", opacity: "1" },
          "100%": { transform: "translateY(-100%)", opacity: "0.1" },
        },
      },
    }
  },
  plugins: [],
}
export default config

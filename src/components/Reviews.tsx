"use client"
import React, { useState, useEffect } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import Image from 'next/image';
import 'swiper/css';
import { Pagination, Autoplay, FreeMode } from 'swiper/modules';

const Reviews = () => {

    const reviews = [   {name: "Oliver a.", position: "singer", image: "/reviews/1.jpg", comment: "Fwicsy boosted my audience, making my music heard far and wide!"},
                        {name: "Emily b.", position: "fashio blogger", image: "/reviews/2.jpg", comment: "With Fwicsy, my fashion blog's reach has doubled - it's a style influencer's dream!"},
                        {name: "Joshua c.", position: "photographer", image: "/reviews/3.jpg", comment: "Thanks to Fwicsy, my photography is now showcased to a much larger audience."},
                        {name: "chris d.", position: "artisan", image: "/reviews/4.jpg", comment: "Fwicsy's growth strategies have brought my artisanal crafts to more appreciative eyes."},
                        {name: "alice w.", position: "travel Blogger", image: "/reviews/5.jpg", comment: "My travel adventures have gained massive traction, all thanks to Fwicsy's effective techniques."},
                        {name: "taylor s.", position: "musician", image: "/reviews/6.jpg", comment: "My fan base has grown exponentially since I started using Fwicsy. It's a game-changer!"},
                        {name: "jordan a.", position: "coffee shop owner", image: "/reviews/7.jpg", comment: "Fwicsy has been pivotal in attracting coffee lovers to my shop's Instagram page."}]

  return (
    <div className='bg-light-white bg-opacity-20 relative overflow-hidden'>
        <Image alt='' src={"/hero_bg.png"} height={0} width={0} sizes='100vw' className='absolute top-0 left-0 h-auto w-full opacity-5 z-[-1]' />
        <div className='container mx-auto h-full xl:px-44 py-20'>
            <h2 className='capitalize text-text text-5xl font-extrabold mb-10 text-center sm:text-left'>Why clients trust <span className='text-primary'>fwicsy</span> Growth</h2>
            <Swiper
                slidesPerView={3}
                centeredSlides={true}
                spaceBetween={30}
                loop={true}
                freeMode={true}
                speed={5000}
                modules={[Pagination, Autoplay, FreeMode]}
                autoplay={{
                    delay: 0,
                    disableOnInteraction: false,
                }}
                className="mySwiper h-full"
                breakpoints={{
                    640: {
                      slidesPerView: 1.5,
                      spaceBetween: 20,
                    },
                    768: {
                      slidesPerView: 2,
                      spaceBetween: 40,
                    },
                    400: {
                        slidesPerView: 1,
                        spaceBetween: 10
                    },
                    300: {
                        slidesPerView: 1,
                        spaceBetween: 10
                    },
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    }
                  }}
            >
            
                {
                    reviews.map(({name, position, image, comment}, index) => (
                        <SwiperSlide key={index} className='bg-dark-white !flex !flex-col !justify-between rounded-xl text-text p-7 !h-80'>
                            <blockquote className='font-bold text-2xl'>
                                {comment}
                            </blockquote>
                            <div className='flex gap-3'>
                                <div className='p-1 border-[1px] border-primary rounded-full'>
                                    <Image className='rounded-full' src={image} alt={`${name}-Review`} height={50} width={50} />
                                </div>
                                <div className='flex flex-col gap-1'>
                                    <p className='font-semibold'>{name}</p>
                                    <span className="text-xs tag font-semi-bold text-text bg-primary px-4 py-1 rounded-full">
                                        {position}
                                    </span>
                                </div>
                            </div>
                        </SwiperSlide>
                    ))
                }

            </Swiper>
        </div>
    </div>
  )
}

export default Reviews
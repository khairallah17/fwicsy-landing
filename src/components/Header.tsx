import React from 'react'
import Hero from './Hero'
import Navbar from './Navbar'
import Image from 'next/image'
import Link from 'next/link'

const Header = () => {

  return (
    <div id="hero" className=' bg-[#e5e7eb] bg-opacity-20 relative overflow-hidden'>
        <Link href="/">
          <Image alt='' src={"/hero_bg.png"} height={0} width={0} sizes='100vw' className='absolute top-0 left-0 h-auto w-full opacity-5 z-[-1]' />
        </Link>
        <Navbar/>
        <Hero/>
    </div>
  )
}

export default Header
"use client"
import React, { useState } from 'react'
import Image from 'next/image'
import tist from "../../public/test.svg"

const Growth = () => {

    const types = ["individuals", "influencers", "business"]
    const typesData = [ 
        {text: "Fwicsy transformed my personal Instagram account. Their hands-off approach meant they did all the follower engagement, helping me build a strong, interactive community. It's genuine growth without the effort! ", testimonial: { image: "/individuels_avatar.jpg", position: "Mary O. (Lifestyle Enthusiast)", content: "Thanks to Fwicsy, I can concentrate on creating content that resonates with my audience, while they handle the growth dynamics. Their strategy has significantly increased my followers and engagement, making my social media journey stress-free and successful. " }},
        {text: "As an influencer, my focus is content, not follower counts. Fwicsy took over the growth aspect seamlessly, boosting my reach and engagement beyond expectations. It’s an influencer’s secret weapon for Instagram success! ", testimonial: {image: "/influencer_avatar.jpg", position: "David G. (Fashion Influencer)", content: "Fwicsy's expertise in audience building lets me dedicate more time to my creative work. Their approach has not only expanded my reach but also connected me with genuine, engaged followers, elevating my influence and opportunities. "}},
        {text: " Running a business is time-consuming enough without worrying about social media growth. Buzzoid stepped in and dramatically increased our Instagram following, directly impacting our sales and customer interactions. A smart investment for any business!", testimonial: {
            content: "With Buzzoid's support, our Instagram platform has flourished, driving more traffic to our products and services. Their efficient growth strategy has freed us to focus on our core business, yielding impressive returns and customer engagement.",
            image: "/business_avatar.jpg", position: "Ashik N. (Startup Founder)"
        }} ]

    const [activeType, setActiveType] = useState(0)

    return (
        <div className='container m-auto my-20 xl:px-44'>
            <h2 className='font-extrabold text-text text-5xl leading-snug capitalize'>Watch your Instagram account
                <br />
                grow — <span className='text-primary'>month after month!</span>
            </h2>
            <div className='flex gap-6 mt-5 flex-wrap'>

            {
                types.map((type, index) => (
                    <button key={index} onClick={() => setActiveType(index)} className={`${activeType == index ? "bg-white border-primary text-primary" : "bg-slate-200 text-slate-500 border-2 hover:border-primary hover:bg-white hover:text-primary"} font-semibold px-8 py-2 text-lg rounded-full duration-200 border-2`}>
                        { type }
                    </button>
                ))
            }

            </div>
            <div className="content grid xmd:grid-cols-2 grid-cols-1 items-center justify-center flex-wrap">

                <div className="content mt-10 text-text pr-10">
                    <p className='text-2xl font-medium'>
                        { typesData[activeType].text }
                    </p>

                    <div className="comment border-l-2 pl-12 mt-10 border-primary">
                        <p className='text-md font-medium'>
                            { typesData[activeType].testimonial.content }
                        </p>

                        <div className="avatar mt-10">
                            <Image src={typesData[activeType].testimonial.image} alt='indivuduels' height={40} width={40} className='rounded-full' />
                            <p className='font-medium text-sm mt-3'>
                                { typesData[activeType].testimonial.position }
                            </p>
                        </div>

                    </div>

                </div>

                <div className="image-content">
                    <Image src="/Growth_1.svg" height={1023} width={829} quality={100} alt="" className='h-full w-full' />
                </div>
            </div>
        </div>
    )
}

export default Growth
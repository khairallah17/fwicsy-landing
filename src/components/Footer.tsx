import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

const Footer = () => {

    const list = ["terms and conditions", "privacy policy"]

    const payment = ["Americanexpress", "Applepay", "Dinersclubcard", "Mastercard", "Visacard"]

  return (
    <div className='bg-light-white bg-opacity-20'>
        <div className='container mx-auto py-12 flex items-center justify-between'>
            <Image src="/logo.png" alt='' height={100} width={150} />
            <ul className='flex items-center gap-4'>
                {
                    list.map((item, index) => (
                        <li key={index}>
                            <Link href={`/${item}`} className="capitalize text-slate-500 hover:text-primary duration-200">
                                {item}
                            </Link>
                        </li>
                    ))
                }
            </ul>
            <ul className='flex gap-2'>
                {
                    payment.map((pay, index) => (
                        <li key={index}>
                            <Image src={`/cards/${pay}.svg`} height={100} width={30} alt={`${pay}`} />
                        </li>
                    ))
                }
            </ul>
        </div>
    </div>
  )
}

export default Footer
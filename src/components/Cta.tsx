import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

const Cta = () => {
    return (
        <div className='container mx-auto'>
            <div className='xl:px-44 py-20'>
                <div className='bg-dark-white rounded-3xl flex justify-between items-center gap-16 py-10 xl:py-20 px-10'>
                    <Image className='xl:flex hidden' src="/cta_1.png" quality={100} loading='lazy' alt="" height={504} width={338} />
                    <div className='flex flex-col gap-4'>
                        <h4 className='text-text font-extrabold text-4xl text-center'>Get Growin&apos; Today</h4>
                        <p className='text-center leading-normal text-lg'>
                        As soon as you place your Buzzoid growth service order, we&apos;ll get to work! If you dream of having a popular, influential, and important Instagram account, we&apos;re ready to get started right now! 
                        </p>
                        <Link href="#hero" className='self-center bg-primary px-12 py-3 capitalize mt-4 hover:shadow-primary hover:shadow-lg duration-300 font-medium text-white rounded-lg'>
                            get started
                        </Link>
                    </div>
                    <Image className='xl:flex hidden' src="/cta-2.png" quality={100} loading='lazy' alt="" height={484} width={308} />
                </div>
            </div>
        </div>
    )
}

export default Cta
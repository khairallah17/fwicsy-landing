import React from 'react'
import Image from 'next/image'
import { IoChatbubbleEllipsesOutline } from "react-icons/io5";
import { FaYoutube } from "react-icons/fa";
import { PiUser, PiUserGearBold } from "react-icons/pi";

const Steps = () => {

  const steps = [ { icon: <IoChatbubbleEllipsesOutline size={50} className=" text-emerald-500 bg-emerald-200 p-3 rounded-full" />, title: "Select a Package", content: "Pick the growth that fits your ambitions best. Our customizable order process is unique in the industry and allows you to specifically define your campaign objectives." },
                  { icon: <FaYoutube size={50} className="text-red-500 bg-red-200 p-3 rounded-full" />, title: "We grab their attention", content: "With your campaign details in hand, we go to work. Using our advanced algorithm and massive community, we engage our followers and audiences to take action on your account. It’s a win win for you and them, because we promote their account in return for engaging with yours." },
                  { icon: <PiUserGearBold size={50} className="text-violet-500 bg-violet-200 rounded-full p-3" />, title: "Watch Your Growth", content: "As our community works its magic, you'll see your account rise in popularity. Witness engagement and follower count soar as your influence expands with Viewpals." }]

  return (
    <div className='container mx-auto py-24 xl:px-44'>
      <h1 className='text-6xl capitalize text-text font-bold'>Get Started in 3 Steps</h1>
      <div className='flex items-center mt-4'>
        <div className="steps w-2/3 flex flex-col gap-8">
          
          {
            steps.map(({icon, title, content}, index) => (
              <div className='w-full flex flex-wrap gap-4 justify-center sm:justify-start shadow-md p-5' key={index}>
                
                <div className="w-fit">
                  {icon}
                </div>
                <div className='w-full flex text-center sm:text-start flex-col'>
                  <h6 className="font-semibold my-3 text-xl">{title}</h6>
                  <p className='text-gray-500'>
                    {content}
                  </p>
                </div>

              </div>
            ))
          }

        </div>
        <div className='w-1/3 flex items-center justify-center'>
          <Image src="/steps.png" className='w-full h-full' alt='' height={500} width={500} quality={100} sizes='100vw' loading='lazy' />
        </div>
      </div>
    </div>
  )
}

export default Steps
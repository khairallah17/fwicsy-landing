"use client"
import { ChangeEvent, useState } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { FaInstagram, FaTiktok, FaYoutube, FaPinterest, FaTwitch, FaSpotify, FaChevronCircleRight, FaChevronCircleLeft  } from "react-icons/fa";
import { FaSquareThreads } from "react-icons/fa6"
import { BsTwitterX } from "react-icons/bs";
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import { Pagination, Navigation } from 'swiper/modules';

const Hero = () => {

    const platforms = ["instagram", "tiktok", "youtube", "pintereset", "twitter", "twitch", "spotify", "threads"]
    const [currentIndex, setCurrentIndex] = useState<number>(0)

    const [username, setUsername] = useState<string>("")

    const media = [ {title: "instagram", image: <FaInstagram size={45} /> },
                    {title: "tiktok", image: <FaTiktok size={45} />},
                    {title: "youtube", image: <FaYoutube size={45} />},
                    {title: "pinterest", image: <FaPinterest size={45} />},
                    {title: "twitter", image: <BsTwitterX size={45} />},
                    {title: "twitch", image: <FaTwitch size={45} />},
                    {title: "spotify", image: <FaSpotify size={45} />},
                    {title: "threads", image: <FaSquareThreads size={45} />} ]

    const handleUsername = (e: ChangeEvent<HTMLInputElement>) => {
        setUsername(e.target.value);
    }

    return (
        <div className='container m-auto flex flex-col gap-6 items-center mt-20 mb-20'>
            <h1 className='text-center text-text text-6xl font-extrabold'>
                Buy Instagram Followers & Likes
                <br />
                <span className='text-primary'>Delivered in Minutes</span>
            </h1>
            <p className=' text-gray-500 text-center'>Promote your content on TikTok, YouTube, Instagram and more...</p>
                <Swiper
                    loop={true}
                    slidesPerView={5}
                    navigation={true}
                    modules={[Pagination, Navigation]}
                    className="mySwiper w-[600px] h-56 overflow-visible mx-44"
                >
                    {
                        media.map(({title, image}, index) => (
                            <SwiperSlide onClick={() => setCurrentIndex(index)} key={index} className='h-full !flex justify-center items-center'>
                                <div className={`relative z-[50] cursor-pointer self-center flex flex-col items-center gap-3 hover:scale-125 duration-300 hover:text-primary ${index == currentIndex ? "scale-125 duration-300 text-primary" : ""}`} key={index}>
                                    <div className="rounded-lg p-6 w-26 h-26 max-h-26 max-w-26 flex flex-col items-center bg-opacity-50 bg-slate-200">
                                        { image }
                                    </div>
                                    <p className="text-gray-400 capitalize">{title}</p>
                                </div>
                            </SwiperSlide>
                        ))
                    }
                </Swiper>
            <div className="search-input bg-white p-2 rounded-lg shadow-primary drop-shadow-xl">
                <input onChange={handleUsername} type="text" name="search" className='px-8 py-3 outline-none' placeholder='@ enter your username' id="" />
                <Link href={`/${platforms[currentIndex]}/${username}`} className='bg-primary px-6 py-4 rounded-md text-white hover:bg-opacity-90 duration-200'>
                    Grow my account
                </Link>
            </div>
        </div>
    )
}

export default Hero
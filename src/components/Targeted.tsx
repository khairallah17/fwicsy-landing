import React from 'react'
import Image from 'next/image'

const Targeted = () => {

    const list = [  "Cultivate a dedicated following within your specific niche",
                    "Engage and connect with audiences genuinely interested in your content",
                    "Foster a dynamic and interactive community of loyal fans" ]

    const list2 = [ "Actively engage with target accounts through views, likes, and comments",
                    "Utilize smart AI to identify and attract your ideal customer demographics",
                    "Leverage our expertise to grow a consistently engaged and expanding follower base" ]

    return (
        <div className='container m-auto flex flex-col gap-24 my-20 xmd:my-28 xl:px-44'>
            <div className='grid xmd:grid-cols-2 justify-items-center xmd:justify-items-start'>
                <Image src="/targeted_bg.png" height={(1023/2)} width={(829/2)} quality={100} loading='lazy' className='mb-10 xmd:mb-0' alt='targeted' />
                <div className="content flex flex-col gap-8">
                    <h2 className='text-text font-extrabold text-5xl'>Targeted Engagement Builds A Real Instagram Community</h2>
                    <p className='text-text font-medium text-lg'> Fwicsy&apos;s growth service doesn&apos;t just supply “numbers.” We find followers who will be interested in your content and will engage regularly with your account! </p>
                    <ul className='text-text font-bold text-lg flex flex-col gap-3'>
                        {
                            list.map((item, index) => (
                                <li key={index} className='flex items-end gap-2'>
                                    <span className='mt-2'>
                                        <Image src="/icons/check.svg" alt='' className='w-8 h-8 min-w-8 min-h-8' height={0} width={0} />
                                    </span>
                                    <span className='text-md'>{item}</span>
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </div>
            <div className="grid xmd:grid-cols-2 justify-items-center xmd:justify-items-end">
                <div className="content flex flex-col gap-8">
                    <h2 className='text-text font-extrabold text-5xl leading-snug'>
                        Human Expertise + Intelligent AI = <span className='text-primary'> Unparalleled Results </span> 
                    </h2>
                    <p className='text-text font-medium text-lg'> 
                        fwicsy&apos;s years of experience with the IG algorithms allow our team and proprietary AI systems to interact with target users 24/7,  bringing new, real followers to your account! 
                    </p>
                    <ul className='text-text font-bold text-lg flex flex-col gap-3'>
                        {
                            list2.map((item, index) => (
                                <li key={index} className='flex items-start gap-2'>
                                    <span className='mt-2'>
                                        <Image src="/icons/check.svg" alt='' className='w-8 h-8' height={0} width={0} />
                                    </span>
                                    <span className='text-md text-text'>{item}</span>
                                </li>
                            ))
                        }
                    </ul>
                </div>
                <Image src="/targeted_2.png" className='self-end mt-10 md:mt-0' alt='' loading='lazy' height={(1032/2)} width={(816/2)} />
            </div>
        </div>
    )
}

export default Targeted
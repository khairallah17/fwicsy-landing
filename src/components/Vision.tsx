import React from 'react'
import Image from 'next/image'

const Vision = () => {

    const bullets = [{ icon: "/icons/snap.png", heading: "It Couldn’t Be Easier", content: "Just provide us with your IG username and define your niche or desired follower type. Then, simply log in to your account, and we'll handle everything else for you!"},
                     { icon: "/icons/target.png", heading: "Choose Your Target Audience", content: "Want followers in a specific industry or geographic region? Want to focus on particular genders or hashtags? Those are the followers you’ll get! " },
                     { icon: "/icons/clock.png", heading: "Working Around the Clock", content: "Our system works to find target followers for you 24 hours a day, exposing your content to them and constantly bringing in new followers!" },
                     { icon: "/icons/verify.png", heading: "100% Real Followers", content: "We operate ethically, within all Instagram rules. Your new followers are all real IG users who choose to follow you. No bots, ever!" },
                     { icon: "/icons/rise.png", heading: "True Organic Growth", content: " As your follower base grows, so does your Instagram influence and importance. Our system creates long-term growth and success!" },
                     { icon: "/icons/easy-to-use.png", heading: "Completely Hands Off", content: " There’s no need to worry about finding new fans. You focus on creating compelling new content — and we do everything else for you!" }]

  return (
    <div className='bg-light-white bg-opacity-20 relative overflow-hidden'>
        <Image alt='' src={"/hero_bg.png"} height={0} width={0} sizes='100vw' className='absolute top-0 left-0 h-auto w-full opacity-5 z-[-1]' />
        <div className='container m-auto py-20 xl:px-44'>
            <h2 className='text-center text-5xl text-text font-extrabold leading-tight'>
                Real, reliable, daily Instagram <br />
                growth that doesn’t stop! 
            </h2>
            <p className='text-center font-medium my-10'>Fwicsy has perfected a safe and effective approach to growth that builds IG
            <br /> popularity and influence over the long term. </p>
            <div className="grid grid-cols-2 grid-rows-3 lg:grid-cols-3 gap-12 sm:grid-rows-2 mt-15">
                {
                    bullets.map(({icon, heading, content}, index) => (
                        <div key={index} className='flex flex-col gap-2'>
                            <Image src={icon} width={50} height={50} alt="" />
                            <h6 className='font-extrabold text-xl capitalize text-text'>{ heading }</h6>
                            <p>{ content }</p>
                        </div>
                    ))
                }
            </div>
        </div>
    </div>
  )
}

export default Vision
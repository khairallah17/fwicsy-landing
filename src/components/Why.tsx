import React from 'react'
import Image from 'next/image'

const Why = () => {
    return (
        <div className='container mx-auto my-24 flex flex-col gap-16'>
            <h1 className='font-bold text-6xl text-text md:text-left texy-center'>Why Fwicsy?</h1>
            <div className='flex flex-wrap items-center justify-center gap-y-5'>
                <div className="data relative min-w-fit w-fit">
                    <div className='absolute sm:left-[205px] left-[153px] top-[-20px] bg-green-200 rounded-full sm:p-6 p-4 w-fit'>
                        <Image src="/icons/trust.svg" alt='' className="w-10 h-10" height={48} width={48} loading='lazy' quality={100} sizes='100vw' />
                    </div>
                    <Image src="/subtract.svg" alt='' height={500} width={500} sizes='100vw' quality={100} loading='lazy' />
                    <div className='absolute sm:top-[110px] top-[90px] flex flex-col items-center gap-4 text-xl px-20 w-full text-center'>
                        <h4 className='font-bold sm:text-2xl text-lg'>
                            Data Driven Growth
                        </h4>
                        <p className='font-normal line-clamp-3 sm:text-lg text-sm text-gray-400'>
                            We use data and analytics to refine and optimize our growth strategies continuously, ensuring the best results for your account.
                        </p>
                    </div>
                </div>
                <div className="data relative min-w-fit w-fit">
                    <div className='absolute sm:left-[205px] left-[153px] top-[-20px] bg-fuchsia-200 rounded-full sm:p-6 p-4 w-fit'>
                        <Image src="/icons/users-trust.svg" className="w-10 h-10" alt='' height={48} width={48} loading='lazy' quality={100} sizes='100vw' />
                    </div>
                    <Image src="/subtract.svg" alt='' height={500} width={500} sizes='100vw' quality={100} loading='lazy' />
                    <div className='absolute sm:top-[110px] top-[90px]  flex flex-col items-center gap-4 text-xl px-20 w-full text-center'>
                        <h4 className='font-bold sm:text-2xl text-lg'>
                        Trusted by Over 24k Clients
                        </h4>
                        <p className=' font-normal line-clamp-3 text-gray-400 sm:text-lg text-sm'>
                            Trusted by thousands of clients around the world, our results speak for themselves. Join over 24k clients who&apos;ve grown their TikTok using our services.
                        </p>
                    </div>
                </div>
                <div className="data relative min-w-fit w-fit">
                    <div className='absolute sm:left-[205px] left-[153px] top-[-20px] bg-blue-200 rounded-full sm:p-6 p-4 w-fit'>
                        <Image src="/icons/user.svg" alt='' className="w-10 h-10" height={48} width={48} loading='lazy' quality={100} sizes='100vw' />
                    </div>
                    <Image src="/subtract.svg" alt='' height={500} width={500} sizes='100vw' quality={100} loading='lazy' />
                    <div className='absolute top-[110px] flex flex-col items-center gap-4 text-xl px-20 w-full text-center'>
                        <h4 className='font-bold sm:text-2xl text-lg'>
                        Dedicated Account Manager
                        </h4>
                        <p className=' font-normal line-clamp-3 text-gray-400 sm:text-lg text-sm'>
                        We realized that what’s rare in this industry is solid customer support ad we vowed to be different. Need help and support? We respond to all emails in 12 hours. Try it for yourself!                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Why
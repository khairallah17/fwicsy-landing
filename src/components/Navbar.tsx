import React from 'react'
import Image from 'next/image'
import Link from 'next/link'

const Navbar = () => {

  const navbar: Array<string> = ["youtube", "tiktok", "pinterest", "instagram", "spotify", "twitch", "threads", "twitter"]

  return (
    <nav className='flex p-5 justify-between items-center bg-transparent'>
      <Link href="/">
        <Image src="/logo.png" height={50} width={200} alt='fwicsy' />
      </Link>

      <div className='flex gap-12'>
        <ul className='xmd:flex gap-4 items-center hidden'>
          {
            navbar.map((item, key) => (
              <li className='capitalize' key={key}>
                <Link href={`/${item.toLowerCase()}`}>
                  {item}
                </Link>
              </li>
            ))
          }
        </ul>
        <Link className='bg-primary py-2 px-4 text-white rounded-lg hover:bg-opacity-90 duration-200' href={"/orders"}>
          My orders
        </Link>
      </div>

    </nav>
  )
}

export default Navbar
import React from 'react'
import Navbar from '../Navbar'
import Footer from '../Footer'
import Why from '../Why'
import Reviews from '../Reviews'
import Steps from '../Steps'
import Image from "next/image"

const PagesLayout = ({children}: React.PropsWithChildren) => {
  return (
    <>
        <Navbar/>
        {children}
        <Why/>
        <Reviews/>
        <Steps/>
        <Footer/>
    </>
  )
}

export default PagesLayout
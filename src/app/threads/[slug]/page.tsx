"use client"
import { useState, useEffect, ChangeEvent } from "react"
import axios from "axios"
import Image from "next/image"
import { PuffLoader } from "react-spinners"
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider';
import { IoMdArrowDropdown } from "react-icons/io";
import { InstagramcommentsNumber, InstagramfollowersNumber, InstagramlikesNumber, InstagramviewsNumber } from "@/contants/prices"
import {
  Accordion,
  AccordionHeader,
  AccordionBody,
} from "@material-tailwind/react";
import { MdKeyboardArrowDown } from "react-icons/md";


export default function Page({ params }: { params: { slug: string } }) {

  const treats = [{image: "/icons/shield.png", content: "Guaranteed Safe Checkout"},
                  {image: "/icons/lock.png", content: "Secure SSL Encryption"},
                  {image: "/icons/like.png", content: "Performance Guaranteed"}]
  
  const [userData ,setUserData] = useState<Object>({})
  const [loading, setLoading] = useState<boolean>(false)

  // useEffect(() => {

  //   const fetchUserData = async () => {

  //     try {

  //       const { data } = await axios.get("https://instagram130.p.rapidapi.com/account-info",{
  //         headers : {
  //           'X-RapidAPI-Key': '0bdc771b17msh90b1da4d2f8a49cp16be7fjsnfb7f80c3ea09',
  //           'X-RapidAPI-Host': 'instagram130.p.rapidapi.com'
  //         },
  //         params: {
  //           'username': params.slug
  //         }
  //       })

  //       setUserData(data)
  //       setLoading(false)

  //     } catch (error: any) {
  //       console.log(error)
  //     }

  //   }

  //   fetchUserData()

  // }, [params.slug])

  
  const [showTooltip, setShowTooltip] = useState<boolean>(false)
  
  const [open, setOpen] = useState<number>(1);
 
  const handleOpen = (value: number) => setOpen(open === value ? 0 : value);

  const valueChange = (event: Event, newValue: number | number[], activeThumb: number) => {
    setFollowers(Number(newValue))
    let price: any = InstagramfollowersNumber.find(n => n.value == newValue)
    setFollowersPrice(Number(price.price))
  }

  const likesValueChange = (event: Event, newValue: number | number[], activeThumb: number) => {
    setLikes(newValue)
    let price: any = InstagramlikesNumber.find(n => n.value == newValue)
    setLikesPrices(Number(price.price))
  }

  const commentsValueChange = (event: Event, newValue: number | number[], activeThumb: number) => {
    setComments(newValue)
    let price: any = InstagramcommentsNumber.find(n => n.value == newValue)
    setCommentsPrice(Number(price.price))
  }

  const viewsChangeValue = (event: Event, newValue: number | number[], activeThumb: number) => {
    setViews(newValue)
    let price: any = InstagramviewsNumber.find(n => n.value == newValue)
    setViewsPrice(Number(price.price))
  }
  
  const [followers, setFollowers] = useState<number | number[]>(500)
  const [followersPrice, setFollowersPrice] = useState<number | number[]>(0)

  const [likes, setLikes] = useState<number | number[]>(0)
  const [likesPrice, setLikesPrices] = useState<number | number[]>(0)

  const [comments, setComments] = useState<number | number[]>(0)
  const [commentsPrice, setCommentsPrice] = useState<number | number[]>(0);

  const [views, setViews] = useState<number | number[]>(0)
  const [viewsPrice, setViewsPrice] = useState<number | number[]>(0)

  const [showOrder, setShowOrder] = useState<boolean>(true)
  const [showPost, setShowPost] = useState<boolean>(false);
  const [showPayment, setShowPayment] = useState<boolean>(false)
  
  
  const [totatPrice, setTotlePrice] = useState<number | number[]>(0)

  return (
        <div className="w-screen p-10 mt-12 flex items-start justify-center overflow-scroll">
          {
            loading ? <PuffLoader color="#FF603C" /> :
            <div className="flex w-[500px] self-center flex-col gap-6 items-center justify-center">

              <div className="flex items-center w-full justify-between gap-4 bg-white shadow-xl p-5 rounded-md">

                <div className="flex items-center justify-center gap-3">
                  <div className="w-16 h-16 rounded-full">
                    <Image src="/user.png" className="rounded-full" layout="responsive" alt="" height={0} width={0} sizes="100vw" loading="lazy" quality={100} />
                  </div>
                  {params.slug}
                </div>

                <div>
                  <p>order details</p>
                  <p></p>
                </div>

              </div>


              <Accordion className="shadow-xl flex flex-col" open={open === 1} icon={<MdKeyboardArrowDown size={32} id={1} open={open} />}>
                <AccordionHeader className={`${open !== 1 && "rounded-b-[5px]"} bg-white px-5 rounded-t-[5px]`} onClick={() => handleOpen(1)}>Make your order</AccordionHeader>
                <AccordionBody className="p-0">
                  <div className={`overflow-hidden duration-300 w-full bg-white shadow-xl p-5 rounded-b-md`}>

                    <div className="selectors">

                      <div className="followers">
                        <div className="flex justify-between w-full">
                          <p>
                            {followers} Followers
                          </p>
                          <p>
                            {Array.isArray(followersPrice) ? followersPrice[0].toFixed(2) : followersPrice.toFixed(2)}$
                          </p>
                        </div>

                        <Box sx={{ width: "100%" }}>
                          <Slider
                            sx={{
                              color: "warning.dark",
                              height: 10
                            }}
                            aria-label="Restricted values"
                            defaultValue={500}
                            onChange={valueChange}
                            step={null}
                            max={5000}
                            marks={InstagramfollowersNumber}
                          />
                        </Box>
                      </div>

                      <div className="likes">
                        <div className="flex justify-between w-full">
                          <p>
                            {likes} Likes
                          </p>
                          <p>
                            {Array.isArray(likesPrice) ? likesPrice[0].toFixed(2) : likesPrice.toFixed(2)}$
                          </p>
                        </div>

                        <Box sx={{ width: "100%" }}>
                          <Slider
                            sx={{
                              color: "warning.dark",
                              height: 10
                            }}
                            aria-label="Restricted values"
                            defaultValue={0}
                            onChange={likesValueChange}
                            step={null}
                            max={5000}
                            marks={InstagramlikesNumber}
                          />
                        </Box>
                      </div>
                      
                      <div className="comments">
                        <div className="flex justify-between w-full">
                          <p>
                            {comments} comments
                          </p>
                          <p>
                            {Array.isArray(commentsPrice) ? commentsPrice[0].toFixed(2) : commentsPrice.toFixed(2)}$
                          </p>
                        </div>

                        <Box sx={{ width: "100%" }}>
                          <Slider
                            sx={{
                              color: "warning.dark",
                              height: 10
                            }}
                            aria-label="Restricted values"
                            defaultValue={0}
                            onChange={commentsValueChange}
                            step={null}
                            max={300}
                            marks={InstagramcommentsNumber}
                          />
                        </Box>
                      </div>
                      
                      <div className="likes">
                        <div className="flex justify-between w-full">
                          <p>
                            {comments} comments
                          </p>
                          <p>
                            {Array.isArray(viewsPrice) ? viewsPrice[0].toFixed(2) : viewsPrice.toFixed(2)}$
                          </p>
                        </div>

                        <Box sx={{ width: "100%" }}>
                          <Slider
                            sx={{
                              color: "warning.dark",
                              height: 10
                            }}
                            aria-label="Restricted values"
                            defaultValue={0}
                            onChange={viewsChangeValue}
                            step={null}
                            max={200000}
                            marks={InstagramviewsNumber}
                          />
                        </Box>
                      </div>

                    </div>

                    <div className="order-details w-full border-b py-4 border-slate-200">
                      <p className="text-left text-slate-500 capitalize">
                        <span>your order </span>
                        {comments ? <span> <b className="text-black">{comments}</b> comments, </span> : ""}
                        {followers ? <span> <b className="text-black">{followers}</b> followers, </span> : ""}
                        {likes ? <span> <b className="text-black">{likes} </b>likes, </span> : ""}
                        {views ? <span> <b className="text-black">{views}</b> views</span> : ""}
                      </p>
                    </div>

                  </div>
                </AccordionBody>
              </Accordion>
              {
                (likes || comments || views) ? 
                <Accordion className="shadow-xl rounded-[5px]" open={open === 2} icon={<MdKeyboardArrowDown size={32} id={2} open={open} />}>
                <AccordionHeader  className={`${open !== 2 && "rounded-b-[5px]"} bg-white px-5 rounded-t-[5px]`} onClick={() => handleOpen(2)}>
                  Select Posts
                </AccordionHeader>
                <AccordionBody className="p-0 bg-white rounded-b-[5px]">
                  <div className="p-5">
                  We&apos;re not always in the position that we want to be at. We&apos;re constantly
                  growing. We&apos;re constantly making mistakes. We&apos;re constantly trying to express
                  ourselves and actualize our dreams.
                  </div>
                </AccordionBody>
                </Accordion> : ""
              }
              <Accordion className="shadow-xl rounded-[5px]" open={open === 3} icon={<MdKeyboardArrowDown size={32} id={3} open={open} />}>
                <AccordionHeader  className={`${open !== 3 && "rounded-b-[5px]"} bg-white px-5 rounded-t-[5px] capitalize`} onClick={() => handleOpen(3)}>
                  confirm Payment
                </AccordionHeader>
                <AccordionBody className="bg-white p-0 rounded-b-[5px] font-normal">
                  <div className="p-5">
                    <form action="" className="flex flex-col gap-4">

                      <div className="flex flex-col gap-2">
                        <label htmlFor="email" className="text-gray-400 text-xs">Please enter your email address. This is where we will send you updates on your fwicsy campaign.</label>
                        <input type="email" className="border border-gray-200 p-2 rounded-md" placeholder="email" name="" id="" required/>
                      </div>

                      <div className="flex items-center gap-1">
                        <input type="checkbox" name="agree" id="" required/>
                        <label className="capitalize text-sm">you agree to our Terms & Refund Policy</label>
                      </div>

                      <button type="submit" className="capitalize bg-orange-500 duration-200 hover:bg-orange-400 py-2 text-white rounded-md font-bold">
                        continue to payment
                      </button>

                      <div className="treats flex items-center">

                        {
                          treats.map(({image, content}, index) => (
                            <div key={index} className="flex flex-col gap-2 py-4 items-center justify-center">
                              <Image src={image} className=" opacity-50" alt="" height={30} width={30} sizes="100vw" quality={100} loading="lazy" />
                              <p className="text-center font-normal">{content}</p>
                            </div>
                          ))
                        }

                      </div>

                    </form>
                  </div>
                </AccordionBody>
              </Accordion>


            </div>
          }
        </div>
  )
}
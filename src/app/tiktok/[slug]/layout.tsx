import Link from "next/link"
import { IoIosArrowBack } from "react-icons/io";
import Image from "next/image";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode,
}) {
  return (
    <section className="">
      <header className="shadow-xl p-5 flex justify-between items-center">
        <Link href="/tiktok" className="flex items-center justify-center text-gray-400">
          <IoIosArrowBack size={18} />
          <p className="capitalize">back</p>
        </Link>
        <Link href="/">
          <Image src="/logo.png" alt="logo" height={250} width={250} sizes="100vw" loading="lazy" quality={100} />
        </Link>
        <div></div>
      </header>
      {children}
    </section>
  )
}
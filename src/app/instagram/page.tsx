"use client"
import React, { ChangeEvent, FormEvent } from 'react'
import { useState } from 'react';
import Image from 'next/image'
import PagesLayout from '@/components/layout/PagesLayout';
import Link from 'next/link';

const Index = () => {

  const spotifyQuery = (e: FormEvent<HTMLFormElement>) => {

  }

  const [username, setUsername] = useState<string>("")

  return (
      <PagesLayout>
        <section className=" overflow-hidden">
          <div className='relative overflow-hidden py-20 !bg-[#e5e7eb] !bg-opacity-20' id="#grow">
            <Image src="/hero_bg.png" className='absolute z-[-99] opacity-20 top-0 w-full md:h-auto h-full ' alt='' height={1920} width={1080} sizes='100vw' quality={100} loading='lazy' />
            <div className="grid lg:grid-cols-2 grid-cols-1 items-center justify-between container mx-auto ">
              <div className="search flex flex-col items-start gap-4">
                <h1 className='font-extrabold md:text-[100px] tracking-tight leading-none capitalize text-7xl'>grow your instagram <br />
                  <span className='font-medium instagram text-transparent !bg-clip-text'>Followers</span>
                </h1>
                <p className='font-light'>24,423+ Creators Trust Fwicsy for Explosive Growth & Engagement</p>
                <search className="search-input flex w-full">
                  <form onSubmit={spotifyQuery} className='p-1 bg-slate-200 rounded-full flex items-center w-full'>
                    <div className='bg-white h-full w-fit flex items-center justify-center pl-5 rounded-l-full'>
                      <Image src="/instagram_icon.svg" alt='' className='md:w-full md:h-full w-8 h-8' height={50} width={50} sizes='100vw' />
                    </div>
                    <input type="text" onChange={(e:ChangeEvent<HTMLInputElement>) => setUsername(e.target.value)} placeholder='enter instagram username here...' className='h-full w-full py-6 px-3 outline-none' name="" id="" />
                    <Link href={`/instagram/${username}`} className='h-full w-fit instagram px-4 py-6 rounded-r-full text-white capitalize flex items-center justify-between font-medium'>
                      <p className='w-[150px]'>
                        grow my instagram
                      </p>
                    </Link>
                  </form>
                </search>
                <ul className='flex items-center gap-4'>
                  <li>
                    <div className='flex items-center gap-2'>
                      <Image src="/list.svg" alt="" height={24} width={24} sizes='100vw' loading='lazy' quality={100} />
                      <p className='font-normal text-sm'>Starting at $4.99. No contracts.</p>
                    </div>
                  </li>
                  <li>
                    <div className='flex items-center gap-2'>
                      <Image src="/list.svg" alt="" height={24} width={24} sizes='100vw' loading='lazy' quality={100} />
                      <p className='font-normal text-sm'>Performance Guaranteed or Money Back!</p>
                    </div>
                  </li>
                </ul>
              </div>
              <Image src="/instagram_bg.png" className='lg:flex hidden h-full' alt="" height={700} width={700} sizes="100vw" loading='lazy' />
            </div>
          </div>
        </section>
      </PagesLayout>
  )
}

export default Index
import React from 'react'
import Header from '@/components/Header'
import Growth from '@/components/Growth'
import Vision from '@/components/Vision'
import Targeted from '@/components/Targeted'
import Reviews from '@/components/Reviews'
import Cta from '@/components/Cta'
import Footer from '@/components/Footer'

const HomePage = () => {
  return (
    <>
        <Header/>
        <Growth/>
        <Vision/>
        <Targeted/>
        <Reviews/>
        <Cta/>
        <Footer/>
    </>
  )
}

export default HomePage
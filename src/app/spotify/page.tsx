"use client"
import React, { ChangeEvent, FormEvent, useState } from 'react'
import Image from 'next/image'
import Link from 'next/link';
import PagesLayout from '@/components/layout/PagesLayout';
import { FaTwitter } from "react-icons/fa";
import { IoMdCheckmarkCircleOutline } from "react-icons/io";
import { FaSpotify } from "react-icons/fa";

const index = () => {

  const setSpotifyUsername = (e: ChangeEvent<HTMLInputElement>) => {
    setUsername(e.target.value)
  }

  const [username, setUsername] = useState<string>("")

  return (
    <PagesLayout>
      <section className="overflow-hidden">
        <div className='relative overflow-hidden py-20 !bg-[#e5e7eb] !bg-opacity-20' id="#grow">
        <Image src="/hero_bg.png" className='absolute z-[-99] opacity-20 top-0 w-full md:h-auto h-full ' alt='' height={1920} width={1080} sizes='100vw' quality={100} loading='lazy' />
        <div className="grid lg:grid-cols-2 grid-cols-1 items-center justify-between container mx-auto ">
          <div className="search flex flex-col items-start gap-4">
            <h1 className='font-extrabold md:text-[100px] tracking-tight leading-none capitalize text-7xl'>grow your spotify <br />
              <span className='font-medium spotify text-transparent !bg-clip-text'>Followers</span>
            </h1>
            <p className='font-light'>24,423+ Creators Trust Fwicsy for Explosive Growth & Engagement</p>
            <search className="search-input flex w-full">
              <form action={`/spotify/${username}`} className='p-1 bg-slate-200 rounded-full flex items-center w-full'>
                <div className='bg-white h-full w-fit flex items-center justify-center pl-5 rounded-l-full'>
                  <FaSpotify size={32} className="text-green-500" />
                </div>
                <input onChange={setSpotifyUsername} type="text" placeholder='enter spotify username here...' className='h-full w-full py-6 px-3 outline-none' name="" id="" />
                <button type='submit' className='h-full w-fit spotify px-4 py-6 rounded-r-full text-white capitalize flex items-center justify-between font-medium'>
                  <p className='w-[150px]'>
                    grow my spotify
                  </p>
                </button>
              </form>
            </search>
            <ul className='flex items-center gap-4'>
              <li>
                <div className='flex items-center gap-2'>
                  <IoMdCheckmarkCircleOutline size={32} className="text-green-500" />
                  <p className='font-normal text-sm'>Starting at $4.99. No contracts.</p>
                </div>
              </li>
              <li>
                <div className='flex items-center gap-2'>
                  <IoMdCheckmarkCircleOutline size={32} className="text-green-500" />
                  <p className='font-normal text-sm'>Performance Guaranteed or Money Back!</p>
                </div>
              </li>
            </ul>
          </div>
          <div className="relative h-full w-full lg:flex hidden">
            <Image className='absolute top-0 right-0' src="/spotify_img.png" alt="" height={750} width={750} sizes="100vw" loading='lazy' />
          </div>
        </div>
      </div>
      </section>
    </PagesLayout>
  )
}

export default index